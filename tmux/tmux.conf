#= Setup ==========================================================================================

set -g default-terminal "screen-256color"
set -sg escape-time 1                           # lower delay time to make tmux responsive
set -g mode-mouse on                            # use the mouse (cheater mode)

# Fix OS X Pasteboard
# set -g default-command "reattach-to-user-namespace -l zsh"

#= Keybindings ====================================================================================

bind r source-file ~/.tmux.conf \;            display "Reloaded ~/.tmux.conf."

# Window splits -----------------------------------------------------------------------------------

bind | split-window -h                        # split window horizontally
bind - split-window -v                        # split window vertically

set -g pane-border-fg black
set -g pane-border-bg black
set -g pane-active-border-fg black
set -g pane-active-border-bg black

# Vim-style pane selection ------------------------------------------------------------------------

# Smart pane switching with awareness of vim splits
bind -n C-h run "(tmux display-message -p '#{pane_current_command}' | grep -iqE '(^|\/)vim(diff)?$' && tmux send-keys C-h) || tmux select-pane -L"
bind -n C-j run "(tmux display-message -p '#{pane_current_command}' | grep -iqE '(^|\/)vim(diff)?$' && tmux send-keys C-j) || tmux select-pane -D"
bind -n C-k run "(tmux display-message -p '#{pane_current_command}' | grep -iqE '(^|\/)vim(diff)?$' && tmux send-keys C-k) || tmux select-pane -U"
bind -n C-l run "(tmux display-message -p '#{pane_current_command}' | grep -iqE '(^|\/)vim(diff)?$' && tmux send-keys C-l) || tmux select-pane -R"
bind -n C-\ run "(tmux display-message -p '#{pane_current_command}' | grep -iqE '(^|\/)vim(diff)?$' && tmux send-keys 'C-\\') || tmux select-pane -l"

bind C-l send-keys 'C-l'

# Vim-style copy & paste --------------------------------------------------------------------------

bind-key -t vi-copy v begin-selection
bind-key -t vi-copy y copy-pipe "reattach-to-user-namespace pbcopy"
unbind   -t vi-copy Enter
bind-key -t vi-copy Enter copy-pipe "reattach-to-user-namespace pbcopy"

#= Windows ========================================================================================

set  -g base-index 1            # start window numbering at 1 (instead of 0)
setw -g pane-base-index 1       # start pane numbering at 1 (instead of 0)
setw -g mode-mouse on           # use mouse to switch panes (cheat mode)

# Resizing
bind -r H resize-pane -L 5
bind -r J resize-pane -D 5
bind -r K resize-pane -U 5
bind -r L resize-pane -R 5

#= Status Bar =====================================================================================

bind-key / set status                   # use Prefix-/ to toggle status bar

set -g status-right-length 100
set -g status-utf8 on                   # enable UTF-8 characters in status bar
set -g status-interval 1                # update once a second

# Colors ------------------------------------------------------------------------------------------

set -g status-fg default
set -g status-bg black

setw -g window-status-current-fg red

set -g status-left-fg brightwhite
set -g status-left-bg red

# Content -----------------------------------------------------------------------------------------

set -g status-left " #S "
set -g status-right '#[fg=brightwhite][%H:%M]#[fg=default] #(battery -pt)'

setw -g window-status-current-format "#[fg=brightwhite][#I]⠗#[fg=default] #W "
setw -g window-status-format "[#I]⠗ #W "

