# Rails
alias b='bundle exec'
alias c="cane --abc-glob '{app,lib,test}/**/*.rb' --abc-max 15 --style-glob '{app,lib}/**/*.rb' --style-measure 100 --no-doc"
alias migrate="rake db:migrate db:test:prepare"
alias rollback="rake db:rollback"
alias t="turn -I\"lib:test\""

# Rake
alias k="rake"


